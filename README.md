# kubernetes-from-scratch

Setting up a kubernetes cluster from scratch. For this you'll need some
machines to work on. I'll just use virtualbox and three ubuntu 18.04 vms for
this. Every vm has Docker Community Edition 18.06 as container engine
installed. Everything but the calico-kube-controller and coredns will be
installed directly on the hosts. Everything that has to run as a service will
be deployed as a systemd service. The files named *.service are the
corresponding unit files. For easy access we'll mount this git repository into
the vms under /kubernetes-from-scratch.

Some of the things that differ wildly in this setup from any production-worthy setup:
- we'll install control plane and worker on the same nodes.
- we'll only talk to the first nodes apiserver just because in a local setup I don't want to install a loadbalancer in front of it.
- we'll install etcd on the nodes too. Usually I would recommend running etcd in its own cluster.
- we're only using one ca without an intermediate ca. In a production environment you would want to isolate your ca with an extra intermediate ca. Also you can use several intermediates e.g. one for etcd, one for the cluster, one for the aggregator api tier.
- also we're putting all config files and certificates in one place here for convenience. It is probably a better idea to seperate them with specific directories for every service at least.
- depending on your use-case you might want to restrict the use of privilegded pods/containers and run docker as a non-root user.
- depending on the scale of your cluster you might want to think about using kube-proxy in IPVS mode and not iptables.
- you'll want some kind of federated authentication for your users so you don't have to manually manage x509 certificates for them. Consider an OIDC server like e.g. [dex](https://github.com/dexidp/dex) or [keycloak](https://www.keycloak.org/) for that.
- if you used the static token file only for bootstrapping the cluster, consider removing the flag from the systemd unit file and restarting your kube-apiservers.
- use PodSecurityPolicies!
- I'm not 100-percent sure, but I think the client certificates don't need to have all the SAN-entries. Probably nicer to leave those out.

Every command in this setup should be run as root, including the openssl commands.

Binaries / Docker images can be found here:
- [Kubernetes Client binaries v1.11.2 (kubectl) - Archive file](https://dl.k8s.io/v1.11.2/kubernetes-client-linux-amd64.tar.gz)
- [Kubernetes Server binaries v1.11.2 (control plane) - Archive file](https://dl.k8s.io/v1.11.2/kubernetes-server-linux-amd64.tar.gz)
- [Kubernetes Node binaries v1.11.2 (worker nodes) - Archive file](https://dl.k8s.io/v1.11.2/kubernetes-node-linux-amd64.tar.gz)
- [etcd binaries v3.3.9 - Archive file](https://github.com/etcd-io/etcd/releases/download/v3.3.9/etcd-v3.3.9-linux-amd64.tar.gz)
- [Calicoctl v3.2.1 - Binary file](https://github.com/projectcalico/calicoctl/releases/download/v3.2.1/calicoctl-linux-amd64)
- [Calico-CNI Plugin v3.2.1 - Binary file](https://github.com/projectcalico/cni-plugin/releases/download/v3.2.1/calico)
- [Calico-CNI IPAM Plugin v3.2.1 - Binary file](https://github.com/projectcalico/cni-plugin/releases/download/v3.2.1/calico-ipam)
- [Standard CNI Plugins v0.7.1 (we'll need the loopback plugin) - Archive file](https://github.com/containernetworking/plugins/releases/download/v0.7.1/cni-plugins-amd64-v0.7.1.tgz)
- [Calico Kube Controllers Docker Image v3.2.1 - Docker image path](quay.io/calico/kube-controllers:v3.2.1)
- [Calico Node Docker Image v3.2.1 - Docker image path](quay.io/calico/node:v3.2.1)
- [CoreDNS v1.2.2 - Docker image repository](https://hub.docker.com/r/coredns/coredns/)

Some great resources can be found here:
- [Tutorial on setting up a root ca](https://jamielinux.com/docs/openssl-certificate-authority/introduction.html)
- [Kubernetes the hard way from Kelsey Hightower](https://github.com/kelseyhightower/kubernetes-the-hard-way)
- [Julia Evans Blog has some detailed articles about some moving parts of Kubernetes](https://jvns.ca/)
- [E.g. this one about the use of certificate authorities in a Kubernetes cluster](https://jvns.ca/blog/2017/08/05/how-kubernetes-certificates-work/)

### Managing Certificates

- Set up a root ca (here we'll use node1 as ca):

   ```shell
   # ssh into node1
   sudo su -
   
   # prepare directories
   mkdir -p /root/ca/{private,csr,certs}
   chmod 700 /root/ca/private

   # creates files for tracking certs and the certificate revocation list
   touch /root/ca/index.txt
   echo 1000 > /root/ca/serial
   echo 1000 > /root/ca/crlnumber

   cd /root/ca
   
   # generate key for root ca certificate
   openssl genrsa -aes256 -out private/ca.key.pem 4096
   chmod 400 private/ca.key.pem
   
   # Enter a pass phrase for the key, then save the pass phrase in a secure
   # location, here we'll use just a file for convenience
   echo -n "mysupersecurepassword" > .kube-ca.secret
   
   # Get openssl.conf from /kubernetes-from-scratch/root-ca
   cp /kubernetes-from-scratch/root-ca/openssl.conf /root/ca
   
   # create root certificate
   openssl req -config openssl.conf \
           -passin file:.kube-ca.secret \
           -key private/ca.key.pem \
           -new -x509 -days 7300 -sha256 -extensions v3_ca \
           -out certs/ca.cert.pem
   
   # verify that everything in the cert is correct
   openssl x509 -noout -text -in certs/ca.cert.pem
   ```

- Copy all OpenSSL config files into the ca directory for easy access

   ```shell
   cp /kubernetes-from-scratch/*/openssl-*.conf /root/ca/
   ```

- Generating a key:

   ```shell
   openssl genrsa -out private/kube-apiserver-server.key.pem 4096
   ```

- Generating a csr:

   ```shell
   openssl req -new -key private/kube-apiserver-server.key.pem \
                    -out csr/kube-apiserver-server.csr.pem \
                    -config openssl-kube-apiserver-server.conf
   ```

- Signing a new certificate:

   ```shell
   openssl x509 -req -in csr/kube-apiserver-server.csr.pem \
                   -passin file:.kube-ca.secret -CA certs/kube-ca.crt.pem \
                   -CAkey private/kube-ca.key.pem -CAcreateserial \
                   -out certs/kube-apiserver-server.crt.pem -days 10000 \
                   -extensions v3_ext -extfile openssl-kube-apiserver-server.conf
   ```

- Verifying the new certificate

   ```shell
   openssl x509 -noout -text -in certs/kube-apiserver-server.crt.pem
   ```

You'll have to do the last three steps for every openssl-*.conf file in this
repo to generate all keys and certificates that we'll use in setting up the
cluster. If you understandibly do not want to do this manually, there is a
script named [generate-all-certs.bash](./generate-all-certs.bash) that assumes
you have a three node cluster with node1, node2 and node3.

### Setting up the Control Plane

#### Some Preparation

- creating all directories we'll need:

   ```shell
   mkdir -p /etc/kubernetes/ssl
   mkdir -p /etc/etcd
   mkdir -p /var/etcd
   ```
- ssh into the 3 nodes with agent-forwarding, so you can cross-login

   ```shell
   # you can login with active agent-forwarding by using ssh -A
   ssh -A youruser@node1.local

   # now you can distribute all the generated certs and keys to the other nodes, e.g.
   scp -r /root/ca/certs/ca.crt.pem youruser@node2.local:
   ```

   The copying is a tedious task, so for the 3 node setup I made a [script](./copy-certs.bash),
   assuming agent-forwarding and authentication via key is enabled for ssh.

- download the binaries for the kubernetes controlplane and etcd

   ```shell
   # download server binaries (kube-apiserver, kube-scheduler, kube-controller-manager)
   curl -LO https://dl.k8s.io/v1.11.2/kubernetes-server-linux-amd64.tar.gz
   tar -xzf kubernetes-server-linux-amd64.tar.gz
   cp kubernetes/server/bin/kube-apiserver /usr/bin/
   cp kubernetes/server/bin/kube-controller-manager /usr/bin/
   cp kubernetes/server/bin/kube-scheduler /usr/bin/
   # we'll install kubectl too for convenience
   cp kubernetes/server/bin/kubectl /usr/bin/
   rm kubernetes-server-linux-amd64.tar.gz
   rm -r kubernetes

   # download etcd
   curl -LO https://github.com/etcd-io/etcd/releases/download/v3.3.9/etcd-v3.3.9-linux-amd64.tar.gz
   tar -xzf etcd-v3.3.9-linux-amd64.tar.gz
   cp etcd-v3.3.9-linux-amd64/etcd /usr/bin/
   cp etcd-v3.3.9-linux-amd64/etcdctl /usr/bin/
   rm etcd-v3.3.9-linux-amd64.tar.gz
   rm -r etcd-v3.3.9-linux-amd64
   ```

#### Deploying etcd

```shell
# These steps have to be repeated on each node. The steps to template the unit
# file are different for each node. Here you see the example for node1.

# install systemd unit file
cp /kubernetes-from-scratch/etcd/etcd.service /etc/systemd/system/

# Template the systemd unit file with the correct IPs and hostname
HOSTNAME=node1
NODE1IP=192.168.2.102
NODE2IP=192.168.2.107
NODE3IP=192.168.2.108
sed -i.bak "s/__HOSTNAME__/$HOSTNAME/g" /etc/systemd/system/etcd.service
sed -i.bak "s/__HOSTIP__/$NODE1IP/g" /etc/systemd/system/etcd.service
sed -i.bak "s/__NODE1IP__/$NODE1IP/g" /etc/systemd/system/etcd.service
sed -i.bak "s/__NODE2IP__/$NODE2IP/g" /etc/systemd/system/etcd.service
sed -i.bak "s/__NODE3IP__/$NODE3IP/g" /etc/systemd/system/etcd.service

# copy configuration and certs to correct location
mv /etc/kubernetes/ssl/etcd-server.crt.pem /etc/etcd/etcd-server.crt.pem
mv /etc/kubernetes/ssl/etcd-server.key.pem /etc/etcd/etcd-server.key.pem
cp /etc/kubernetes/ssl/ca.crt.pem /etc/etcd/

# enable and start the etcd service
systemctl enable etcd.service
systemctl start etcd.service
```

#### Deploying kube-apiserver

```shell
# install systemd unit file
cp /kubernetes-from-scratch/kube-apiserver/kube-apiserver.service /etc/systemd/system/
cp /kubernetes-from-scratch/kube-apiserver/token-auth-file.csv /etc/kubernetes/token-auth-file.csv

# Template the systemd unit file with the correct IP
HOSTIP=192.168.2.102
NODE1IP=192.168.2.102
NODE2IP=192.168.2.107
NODE3IP=192.168.2.108
sed -i.bak "s/__HOSTIP__/$HOSTIP/g" /etc/systemd/system/kube-apiserver.service
sed -i.bak "s/__NODE1IP__/$NODE1IP/g" /etc/systemd/system/kube-apiserver.service
sed -i.bak "s/__NODE2IP__/$NODE2IP/g" /etc/systemd/system/kube-apiserver.service
sed -i.bak "s/__NODE3IP__/$NODE3IP/g" /etc/systemd/system/kube-apiserver.service

# start service
systemctl enable kube-apiserver.service
systemctl start kube-apiserver.service
```

#### Deploying kube-controller-manager

```shell
# install systemd unit file
cp /kubernetes-from-scratch/kube-controller-manager/kube-controller-manager.service /etc/systemd/system/

# copy configuration to correct location
cp /kubernetes-from-scratch/kube-controller-manager/kube-controller-manager.kubeconfig /etc/kubernetes/

# Template the systemd unit file and kubeconfig file with the correct IP
NODE1IP=192.168.2.102
sed -i.bak "s/__NODE1IP__/$NODE1IP/g" /etc/systemd/system/kube-controller-manager.service
sed -i.bak "s/__NODE1IP__/$NODE1IP/g" /etc/kubernetes/kube-controller-manager.kubeconfig

# start service
systemctl enable kube-controller-manager
systemctl start kube-controller-manager
```

#### Deploying kube-scheduler

```shell
# install systemd unit file
cp /kubernetes-from-scratch/kube-scheduler/kube-scheduler.service /etc/systemd/system/

# copy configuration to correct location
cp /kubernetes-from-scratch/kube-scheduler/kube-scheduler-config.yaml /etc/kubernetes/
cp /kubernetes-from-scratch/kube-scheduler/kube-scheduler.kubeconfig /etc/kubernetes/

# Template the kubeconfig file with the correct IP
NODE1IP=192.168.2.102
sed -i.bak "s/__NODE1IP__/$NODE1IP/g" /etc/kubernetes/kube-scheduler.kubeconfig

# start service
systemctl enable kube-scheduler
systemctl start kube-scheduler
```

#### Creating admin kubeconfig file for kubectl

```shell
mkdir -p /root/.kube
cp /kubernetes-from-scratch/admin.kubeconfig /root/.kube/config

# test if it works
kubectl cluster-info
```

### Setting up Worker Nodes

#### Some Preparation

- creating all directories we'll need:

   ```shell
   mkdir -p /etc/kubernetes/ssl/private
   mkdir -p /var/lib/{kubelet,kube-proxy}
   mkdir -p /etc/systemd/system/docker.service.d
   ```

- installing Docker Community Edition and overriding standard unit file

   ```shell
   apt update && apt install apt-transport-https ca-certificates software-properties-common

   echo -n "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable" > /etc/apt/sources.list.d/docker.list

   curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

   apt update && apt install docker-ce

   # override docker standard systemd unit
   systemctl stop docker.service
   cp /kubernetes-from-scratch/docker/docker.service-override.conf /etc/systemd/system/docker.service.d/

   # cleanup docker iptables rules and network interfaces
   iptables -t nat -F
   ip link set docker0 down
   ip link delete docker0

   # start docker again
   systemctl daemon-reload
   systemctl start docker.service
   ```

- download the binaries for the kubernetes workers

   ```shell
   # download server binaries (kube-proxy, kubelet) and install them
   curl -LO https://dl.k8s.io/v1.11.2/kubernetes-node-linux-amd64.tar.gz
   tar -xzf kubernetes-server-linux-amd64.tar.gz
   cp kubernetes/node/bin/kubelet /usr/bin/
   cp kubernetes/node/bin/kube-proxy /usr/bin
   ```

#### Deploying the kubelet

- Configuring role-based access control for kubelet to apiserver communication

```shell
kubectl apply -f /kubernetes-from-scratch/kubelet/kubelet-rbac.yaml
```

The kube-apiserver needs access to its own api, but only for the node
resources, that are provided by the kubelet api, in order to enable metrics
collection, proxying, node statistics and more. That is also the reason why
the kube-apiserver-server.crt.pem certificate has not only serverAuth but
also clientAuth extensions enabled.

- Configuring and starting the kubelet

```shell
# install systemd unit file
cp /kubernetes-from-scratch/kubelet/kubelet.service /etc/systemd/system/

# copy configuration to the correct location
cp /kubernetes-from-scratch/kubelet/kubelet-config.yaml /etc/kubernetes/
cp /kubernetes-from-scratch/kubelet/kubelet.kubeconfig /etc/kubernetes/
# The following templating commands are specific to the node you're running 
# them on. They have to be run on every worker node. I use node1 as example.
# On other nodes the __HOSTXYZ__ fields have to become specific to the node
# while the __NODE1IP__ stays the IP of node1
HOSTNAME=node1
NODE1IP=192.168.102
sed -i.bak "s/__HOSTIP__/$NODE1IP/g" /etc/systemd/system/kubelet.service
sed -i.bak "s/__HOSTNAME__/$HOSTNAME/g" /etc/systemd/system/kubelet.service
sed -i.bak "s/__HOSTNAME__/$HOSTNAME/g" /etc/kubernetes/kubelet-config.yaml
sed -i.bak "s/__HOSTNAME__/$HOSTNAME/g" /etc/kubernetes/kubelet.kubeconfig
sed -i.bak "s/__NODE1IP__/$NODE1IP/g" /etc/kubernetes/kubelet.kubeconfig

# start service
systemctl enable kubelet.service
systemctl start kubelet.service
```

#### Deploying kube-proxy

```shell
# install systemd unit file
cp /kubernetes-from-scratch/kube-proxy/kube-proxy.service /etc/systemd/system/

# copy configuration and certs to correct location
cp /kubernetes-from-scratch/kube-proxy/kube-proxy.kubeconfig /etc/kubernetes/

# template the files with the IP of node1 as apiserver address
NODE1IP=192.168.102
sed -i.bak "s/__NODE1IP__/$NODE1IP/g" /etc/kubernetes/kube-proxy.kubeconfig
sed -i.bak "s/__NODE1IP__/$NODE1IP/g" /etc/systemd/system/kube-proxy.service

# start service
systemctl enable kube-proxy.service
systemctl start kube-proxy.service
```

#### Testing

```shell
# test if all nodes have registered successfully with kube-apiserver
kubectl get nodes -o wide

# troubleshooting
# LOGS!
journalctl -u kubelet.service
journalctl -u kube-apiserver.service
journalctl ...
```

### Setting up calico networking

#### Some Preparation

- creating all directories we'll need:

   ```shell
   mkdir -p /etc/cni/net.d
   mkdir -p /opt/cni/bin
   mkdir -p /var/lib/calico
   mkdir -p /var/run/calico
   ```

#### Installing calico CNI Plugins

- download the binaries

   ```shell
   # Download th calico CNI plugin
   curl -L https://github.com/projectcalico/cni-plugin/releases/download/v3.2.1/calico -o /opt/cni/bin/calico

   # Download the calico-ipam CNI plugin (used to manage IP addresses in your cluster)
   curl -L https://github.com/projectcalico/cni-plugin/releases/download/v3.2.1/calico-ipam -o /opt/cni/bin/calico-ipam
   ```

- make sure they're executable

   ```shell
   chmod o+x /opt/cni/bin/*
   ```

- configure the plugins

Just as an example calico uses a static token here in its kubeconfig file, that
exists in the --static-token-file file of the kube-apiserver. In a production
environment it is probably better to make this a serviceaccount token or
another x509 certificate pair.

   ```shell
   # copy config files
   cp /kubernetes-from-scratch/calico/10-calico.conf /etc/cni/net.d/
   cp /kubernetes-from-scratch/calico/calico-cni.kubeconfig /etc/kubernetes/

   # template the config files
   NODE1IP=192.168.2.102
   NODE2IP=192.168.2.107
   NODE3IP=192.168.2.108
   sed -i.bak "s/__NODE1IP__/$NODE1IP/g" /etc/cni/net.d/10-calico.conf
   sed -i.bak "s/__NODE2IP__/$NODE2IP/g" /etc/cni/net.d/10-calico.conf
   sed -i.bak "s/__NODE3IP__/$NODE3IP/g" /etc/cni/net.d/10-calico.conf
   sed -i.bak "s/__NODE1IP__/$NODE1IP/g" /etc/kubernetes/calico-cni.kubeconfig
   ```

#### Installing CNI Loopback Plugin

- download the binaries

   ```shell
   curl -LO https://github.com/containernetworking/plugins/releases/download/v0.7.1/cni-plugins-amd64-v0.7.1.tgz
   tar -xzf cni-plugins-amd64-v0.7.1.tgz
   rm cni-plugins-amd64-v0.7.1.tgz
   ```

- copy them to /opt/cni/bin

   ```shell
   mv cni-plugins-amd64-v0.7.1/loopback /opt/cni/bin/
   rm -r cni-plugins-amd64-v0.7.1
   ```

#### Installing calicoctl

```shell
curl -L https://github.com/projectcalico/calicoctl/releases/download/v3.2.1/calicoctl -o /usr/bin/calicoctl

chmod +x /usr/bin/calicoctl
```

#### Deploying calico-node

We'll start the calico-node container via the calicoctl cli tool. In a
production setup the container could very well be started just like this or
even as a Kubernetes DaemonSet with access to the HostNetwork.
Don't be alarmed by the constant restarting of the service. The calico node
container executes a graceful restart after every BGP sync. It's not nice but
that's how it seems to work.

```shell
# install systemd unit file
cp /kubernetes-from-scratch/calico/calico-node.service /etc/systemd/system/

# template the unit file (again on every node, with __HOSTXYZ__ vars being node-specific)
HOSTNAME=node1
NODE1IP=192.168.2.102
NODE2IP=192.168.2.107
NODE3IP=192.168.2.108
sed -i.bak "s/__NODE1IP__/$NODE1IP/g" /etc/systemd/system/calico-node.service
sed -i.bak "s/__NODE2IP__/$NODE2IP/g" /etc/systemd/system/calico-node.service
sed -i.bak "s/__NODE3IP__/$NODE3IP/g" /etc/systemd/system/calico-node.service
sed -i.bak "s/__HOSTIP__/$NODE1IP/g" /etc/systemd/system/calico-node.service
sed -i.bak "s/__HOSTNAME__/$HOSTNAME/g" /etc/systemd/system/calico-node.service

# start service
systemctl enable calico-node.service
systemctl start calico-node.service
```

#### Deploying calico-kube-controllers

```shell
cp /kubernetes-from-scratch/calico/calico-kube-controller.yaml /etc/kubernetes/


NODE1IP=192.168.2.102
NODE2IP=192.168.2.107
NODE3IP=192.168.2.108
sed -i.bak "s/__NODE1IP__/$NODE1IP/g" /etc/kubernetes/calico-kube-controller.yaml
sed -i.bak "s/__NODE2IP__/$NODE2IP/g" /etc/kubernetes/calico-kube-controller.yaml
sed -i.bak "s/__NODE3IP__/$NODE3IP/g" /etc/kubernetes/calico-kube-controller.yaml

kubectl apply -f /etc/kubernetes/calico-kube-controller.yaml

kubectl get pods -n kube-system
```

### Deploying CoreDNS and Kubernetes Dashboard

Since now we have a running Kubernetes cluster with cluster networking
installed, we can deploy our first apps on it. We do not yet have any form of
cluster-internal DNS set up, so we'll do that first with CoreDNS.

#### CoreDNS

- configure CoreDNS

   ```shell
   cp /kubernetes-from-scratch/coredns/coredns.yaml /etc/kubernetes/
   # templating the CoreDNS config
   NODE1IP=192.168.2.102
   sed -i.bak "s/__NODE1IP__/$NODE1IP/g" /etc/kubernetes/coredns.yaml
   ```

- deploy CoreDNS

   ```shell
   kubectl apply -f /etc/kubernetes/coredns.yaml

   # test if it worked
   kubectl get pods -n kube-system -o wide
   ```

#### Kubernetes Dashboard

And last but not least let's deploy the Kubernetes Dashboard to have a more
visual overview of our shiny new cluster.

```shell
cp /kubernetes-from-scratch/kubernetes-dashboard/kubernetes-dashboard.yaml /etc/kubernetes/

kubectl apply -f /etc/kubernetes/kubernetes-dashboard.yaml

# have look if it is starting
kubectl get pods -n kube-system -o wide

# and now have a look at the dashboard itself
kubectl proxy

# now you can point your browser at http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy
# and login with your admin.kubeconfig file
```

For working metrics resource endpoints in the kube-apiserver we can also deploy
the metrics-server. Since the Kubernetes Dashboard is not supporting the new
metrics-server as of yet, we can also deploy the deprecated heapster service.

```shell
cp /kubernetes-from-scratch/metrics-server/* /etc/kubernetes/

kubectl apply -f /etc/kubernetes/metrics-server.yaml
kubectl apply -f /etc/kubernetes/heapster.yaml

# now your Kubernetes Dashboard should begin to display cpu and memory graphs
# after you waited for a bit
```

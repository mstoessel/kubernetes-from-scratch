#!/usr/bin/env bash
# ---
# This script copies the certs and private keys to the other 2 cluster nodes.
# This script assumes being executed on node1, where all scripts were created.
# The first argument is the username.
# The last 2 arguments are the 2 other nodes IPs
#
# Author: Manuel Stößel <manuel.stoessel@eventim.de>
# ---

set -e
shopt -s extglob

BASEDIR=$(dirname "$0")

USERNAME="$1"

# next 2 arguments are the IP addresses of the 3 nodes in ascending order
NODE2IP="$2"
NODE3IP="$3"

cp /root/ca/certs/* /etc/kubernetes/ssl/
scp /root/ca/certs/* $USERNAME@$NODE2IP:/etc/kubernetes/ssl/
scp /root/ca/certs/* $USERNAME@$NODE3IP:/etc/kubernetes/ssl/

cp /root/ca/private/!(ca.key.pem|kubelet-node2.key.pem|kubelet-node3.key.pem) /etc/kubernetes/ssl/
scp /root/ca/private/!(ca.key.pem|kubelet-node1.key.pem|kubelet-node3.key.pem) $USERNAME@$NODE2IP:/etc/kubernetes/ssl/
scp /root/ca/private/!(ca.key.pem|kubelet-node1.key.pem|kubelet-node2.key.pem) $USERNAME@$NODE3IP:/etc/kubernetes/ssl/
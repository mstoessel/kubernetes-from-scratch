#!/usr/bin/env bash
# ---
# This script generates all certificates used in the example for the DevOpsDays
# Berlin 2018. It's a 3 node cluster where the FQDNs are node1.local,
# node2.local and node3.local.
# This script has to be executed in the toplevel directory of the root ca. In
# this example this would be /root/ca/
#
# Author: Manuel Stößel <manuel.stoessel@eventim.de>
# ---

set -e
shopt -s extglob

BASEDIR=$(dirname "$0")
OPENSSL=$(command -v openssl)

if [ -z $OPENSSL ]; then
    echo "ERROR: Can't find OpenSSL. Make sure openssl cli is installed and in PATH!"
    exit 1
fi

SUDO=''
if (( $EUID != 0 )); then
    SUDO='sudo'
fi

# has to be master, worker, calico or coredns
NODETYPE="$1"

# next 3 arguments are the IP addresses of the 3 nodes in ascending order
NODE1IP="$2"
NODE2IP="$3"
NODE3IP="$4"

HOSTS="node1 node2 node3"



# template IPs into openssl config files
for conf in openssl-*.conf; do
    sed -i.bak "s/__NODE1IP__/$NODE1IP/g" $conf
    sed -i.bak "s/__NODE2IP__/$NODE2IP/g" $conf
    sed -i.bak "s/__NODE3IP__/$NODE3IP/g" $conf
done


case $NODETYPE in
"master")
    # create all certificates for a master node
    # etcd server cert
    $SUDO $OPENSSL genrsa -out private/etcd-server.key.pem 4096
    $SUDO $OPENSSL req -new -key private/etcd-server.key.pem \
                    -out csr/etcd-server.csr.pem \
                    -config openssl-etcd-server.conf
    $SUDO $OPENSSL x509 -req -in csr/etcd-server.csr.pem \
                   -passin file:.kube-ca.secret -CA certs/ca.crt.pem \
                   -CAkey private/ca.key.pem -CAcreateserial \
                   -out certs/etcd-server.crt.pem -days 10000 \
                   -extensions v3_ext -extfile openssl-etcd-server.conf

    # kube-apiserver server cert and certificate chain file
    $SUDO $OPENSSL genrsa -out private/kube-apiserver-server.key.pem 4096
    $SUDO $OPENSSL req -new -key private/kube-apiserver-server.key.pem \
                    -out csr/kube-apiserver-server.csr.pem \
                    -config openssl-kube-apiserver-server.conf
    $SUDO $OPENSSL x509 -req -in csr/kube-apiserver-server.csr.pem \
                   -passin file:.kube-ca.secret -CA certs/ca.crt.pem \
                   -CAkey private/ca.key.pem -CAcreateserial \
                   -out certs/kube-apiserver-server.crt.pem -days 10000 \
                   -extensions v3_ext -extfile openssl-kube-apiserver-server.conf
    # concatenate kube-apiserver cert with root ca cert
    cat certs/kube-apiserver-server.crt.pem certs/ca.crt.pem > certs/kube-apiserver-cert-chain.crt.pem
    
    # kube-apiserver client cert
    $SUDO $OPENSSL genrsa -out private/kube-apiserver-client.key.pem 4096
    $SUDO $OPENSSL req -new -key private/kube-apiserver-client.key.pem \
                    -out csr/kube-apiserver-client.csr.pem \
                    -config openssl-kube-apiserver-client.conf
    $SUDO $OPENSSL x509 -req -in csr/kube-apiserver-client.csr.pem \
                   -passin file:.kube-ca.secret -CA certs/ca.crt.pem \
                   -CAkey private/ca.key.pem -CAcreateserial \
                   -out certs/kube-apiserver-client.crt.pem -days 10000 \
                   -extensions v3_ext -extfile openssl-kube-apiserver-client.conf

    # kube-apiserver etcd cert
    $SUDO $OPENSSL genrsa -out private/kube-apiserver-etcd.key.pem 4096
    $SUDO $OPENSSL req -new -key private/kube-apiserver-etcd.key.pem \
                    -out csr/kube-apiserver-etcd.csr.pem \
                    -config openssl-kube-apiserver-etcd.conf
    $SUDO $OPENSSL x509 -req -in csr/kube-apiserver-etcd.csr.pem \
                   -passin file:.kube-ca.secret -CA certs/ca.crt.pem \
                   -CAkey private/ca.key.pem -CAcreateserial \
                   -out certs/kube-apiserver-etcd.crt.pem -days 10000 \
                   -extensions v3_ext -extfile openssl-kube-apiserver-etcd.conf
    
    # kube-apiserver proxyclient cert
    $SUDO $OPENSSL genrsa -out private/kube-apiserver-proxyclient.key.pem 4096
    $SUDO $OPENSSL req -new -key private/kube-apiserver-proxyclient.key.pem \
                    -out csr/kube-apiserver-proxyclient.csr.pem \
                    -config openssl-kube-apiserver-proxyclient.conf
    $SUDO $OPENSSL x509 -req -in csr/kube-apiserver-proxyclient.csr.pem \
                   -passin file:.kube-ca.secret -CA certs/ca.crt.pem \
                   -CAkey private/ca.key.pem -CAcreateserial \
                   -out certs/kube-apiserver-proxyclient.crt.pem -days 10000 \
                   -extensions v3_ext -extfile openssl-kube-apiserver-proxyclient.conf
    
    # kube-controller-manager client cert
    $SUDO $OPENSSL genrsa -out private/kube-controller-manager-client.key.pem 4096
    $SUDO $OPENSSL req -new -key private/kube-controller-manager-client.key.pem \
                    -out csr/kube-controller-manager-client.csr.pem \
                    -config openssl-kube-controller-manager-client.conf
    $SUDO $OPENSSL x509 -req -in csr/kube-controller-manager-client.csr.pem \
                   -passin file:.kube-ca.secret -CA certs/ca.crt.pem \
                   -CAkey private/ca.key.pem -CAcreateserial \
                   -out certs/kube-controller-manager-client.crt.pem -days 10000 \
                   -extensions v3_ext -extfile openssl-kube-controller-manager-client.conf
    
    # kube-controller-manager serviceaccounts cert
    $SUDO $OPENSSL genrsa -out private/kube-controller-manager-serviceaccounts.key.pem 4096
    $SUDO $OPENSSL req -new -key private/kube-controller-manager-serviceaccounts.key.pem \
                    -out csr/kube-controller-manager-serviceaccounts.csr.pem \
                    -config openssl-kube-controller-manager-serviceaccounts.conf
    $SUDO $OPENSSL x509 -req -in csr/kube-controller-manager-serviceaccounts.csr.pem \
                   -passin file:.kube-ca.secret -CA certs/ca.crt.pem \
                   -CAkey private/ca.key.pem -CAcreateserial \
                   -out certs/kube-controller-manager-serviceaccounts.crt.pem -days 10000 \
                   -extensions v3_ext -extfile openssl-kube-controller-manager-serviceaccounts.conf
    
    # kube-scheduler client cert
    $SUDO $OPENSSL genrsa -out private/kube-scheduler-client.key.pem 4096
    $SUDO $OPENSSL req -new -key private/kube-scheduler-client.key.pem \
                    -out csr/kube-scheduler-client.csr.pem \
                    -config openssl-kube-scheduler-client.conf
    $SUDO $OPENSSL x509 -req -in csr/kube-scheduler-client.csr.pem \
                   -passin file:.kube-ca.secret -CA certs/ca.crt.pem \
                   -CAkey private/ca.key.pem -CAcreateserial \
                   -out certs/kube-scheduler-client.crt.pem -days 10000 \
                   -extensions v3_ext -extfile openssl-kube-scheduler-client.conf
    ;;
"worker")
    # create all certificates for a worker node
    # first create all kubelet node-specific certs
    for host in $HOSTS; do
        $SUDO $OPENSSL genrsa -out private/kubelet-$host.key.pem 4096
        $SUDO $OPENSSL req -new -key private/kubelet-$host.key.pem \
                    -out csr/kubelet-$host.csr.pem \
                    -config openssl-kubelet-$host.conf
        $SUDO $OPENSSL x509 -req -in csr/kubelet-$host.csr.pem \
                   -passin file:.kube-ca.secret -CA certs/ca.crt.pem \
                   -CAkey private/ca.key.pem -CAcreateserial \
                   -out certs/kubelet-$host.crt.pem -days 10000 \
                   -extensions v3_ext -extfile openssl-kubelet-$host.conf
    done

    # kube-proxy client cert
    $SUDO $OPENSSL genrsa -out private/kube-proxy-client.key.pem 4096
    $SUDO $OPENSSL req -new -key private/kube-proxy-client.key.pem \
                    -out csr/kube-proxy-client.csr.pem \
                    -config openssl-kube-proxy-client.conf
    $SUDO $OPENSSL x509 -req -in csr/kube-proxy-client.csr.pem \
                   -passin file:.kube-ca.secret -CA certs/ca.crt.pem \
                   -CAkey private/ca.key.pem -CAcreateserial \
                   -out certs/kube-proxy-client.crt.pem -days 10000 \
                   -extensions v3_ext -extfile openssl-kube-proxy-client.conf
    ;;
"calico")
    # calico etcd cert
    $SUDO $OPENSSL genrsa -out private/calico-etcd.key.pem 4096
    $SUDO $OPENSSL req -new -key private/calico-etcd.key.pem \
                    -out csr/calico-etcd.csr.pem \
                    -config openssl-calico-etcd.conf
    $SUDO $OPENSSL x509 -req -in csr/calico-etcd.csr.pem \
                   -passin file:.kube-ca.secret -CA certs/ca.crt.pem \
                   -CAkey private/ca.key.pem -CAcreateserial \
                   -out certs/calico-etcd.crt.pem -days 10000 \
                   -extensions v3_ext -extfile openssl-calico-etcd.conf
    ;;
"coredns")
    # coredns client cert
    $SUDO $OPENSSL genrsa -out private/coredns-client.key.pem 4096
    $SUDO $OPENSSL req -new -key private/coredns-client.key.pem \
                    -out csr/coredns-client.csr.pem \
                    -config openssl-coredns-client.conf
    $SUDO $OPENSSL x509 -req -in csr/coredns-client.csr.pem \
                   -passin file:.kube-ca.secret -CA certs/ca.crt.pem \
                   -CAkey private/ca.key.pem -CAcreateserial \
                   -out certs/coredns-client.crt.pem -days 10000 \
                   -extensions v3_ext -extfile openssl-coredns-client.conf
    ;;
*)
    echo "First argument has to be 'master', 'worker', 'calico' or 'coredns'!"
    exit 1
    ;;
esac

